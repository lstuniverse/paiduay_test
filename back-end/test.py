from flask import Flask, request, jsonify, flash
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
import sqlite3
import sys
import json

app = Flask(__name__)
# Enable CORS
CORS(app)


@app.route('/')
def Home():
    return "<h1>Hello World!</h1>"


@app.route("/getall", methods=["GET"])
def getall():
    conn = sqlite3.connect('user.db')

    with conn:
        cur = conn.cursor()
        cur.execute("SELECT * FROM users")

        rows = cur.fetchall()

        json_data_list = []
        for row in rows:
            json_data_list.append(row)
    return (json.dumps(json_data_list))


@app.route("/get/<int:id>", methods=["GET"])
def getUserById(id):
    try:
        conn = sqlite3.connect('user.db')
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM users WHERE id=?", (id,))
        row = cursor.fetchone()
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route("/getusername/<first_name>", methods=["GET"])
def getUserByName(first_name):

    try:
        conn = sqlite3.connect('user.db')
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM users WHERE first_name=?", (first_name,))
        row = cursor.fetchone()
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route("/getlastname/<last_name>", methods=["GET"])
def getUserByLastName(last_name):
    try:
        conn = sqlite3.connect('user.db')
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM users WHERE last_name=?", (last_name,))
        row = cursor.fetchone()
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route("/getemail/<email>", methods=["GET"])
def getUserByEmail(email):
    try:
        conn = sqlite3.connect('user.db')
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM users WHERE email=?", (email,))
        row = cursor.fetchone()
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route("/getgender/<gender>", methods=["GET"])
def getUserByGender(gender):
    try:
        conn = sqlite3.connect('user.db')
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM users WHERE gender=?", (gender,))
        rows = cursor.fetchall()

        json_data_list = []
        for row in rows:
            json_data_list.append(row)

        resp = jsonify(json_data_list)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route("/getage/<int:age>", methods=["GET"])
def getUserByAgeOfRange(age):
    try:
        conn = sqlite3.connect('user.db')
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM users WHERE age=?", (age,))
        rows = cursor.fetchall()

        json_data_list = []
        for row in rows:
            json_data_list.append(row)

        resp = jsonify(json_data_list)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route("/create", methods=["POST"])
def add_user():

    # resp = jsonify(get_id,fname, lname, get_email, get_gender, get_age)
    # return resp

    try:
        get_id = request.form['id']
        fname = request.form['first_name']
        lname = request.form['last_name']
        get_email = request.form['email']
        get_gender = request.form['gender']
        get_age = request.form['age']

        if get_id and fname and lname and get_email and get_gender and get_age and request.method == 'POST':
            sql = "INSERT INTO users(id, first_name, last_name, email, gender, age) VALUES(?,?,?,?,?,?)"
            data = (get_id, fname, lname, get_email, get_gender, get_age)
            conn = sqlite3.connect('user.db')
            cursor = conn.cursor()
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('User Created successfully!')
            resp.status_code = 200
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/update', methods=['PUT'])
def update_user():
    try:
        get_id = request.form['id']
        fname = request.form['first_name']
        lname = request.form['last_name']
        get_email = request.form['email']
        get_gender = request.form['gender']
        get_age = request.form['age']

        if request.method == 'PUT':
            sql = "UPDATE Set first_name=?, last_name=?, email=?, gender=?, age=? WHERE id=?"
            data = (fname, lname, get_email, get_gender, get_age, get_id)
            conn = sqlite3.connect('user.db')
            cursor = conn.cursor()
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify('User updated successfully!')
            resp.status_code = 200
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.route('/delete/<int:id>', methods=['DELETE'])
def delete_user(id):
    try:
        conn = sqlite3.connect('user.db')
        cursor = conn.cursor()
        cursor.execute("DELETE FROM users WHERE id=?", (id,))
        conn.commit()
        resp = jsonify('User deleted successfully!')
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
    finally:
        cursor.close()
        conn.close()


@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=8000)

import re


class Check:
    def __init__(self, input):
        self.input = input

    def verify_text(self, text_input):
        count1 = 0
        count2 = 0
        count3 = 0
        count4 = 0

        pattern = re.compile(
            r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
        # pattern = re.compile("^([A-Z][0-9]+)+$")
        for str in text_input:
            if (str.isupper()) == True:
                count1 += 1

            elif (str.islower()) == True:
                count2 += 1

            elif (str.isdigit()) == True:
                count3 += 1

            else:

                if pattern.search(str):
                    print('Not found')
                else:
                    count4 += 1
                    # print(count4)

            # print(count1, count2, count3, count4)

        if count2 == 0 and count3 == 0 and count4 == 0:
            print("All Capital Letter ")
        elif count1 == 0 and count3 == 0 and count4 == 0:
            print("All Small Letter ")
        elif count1 != 0 and count2 != 0 and count3 == 0 and count4 == 0:
            print("Mix ")
        else:
            print("Invalid Input ")


if __name__ == "__main__":
    text_input = input("Enter your Text: ")
    case = Check(text_input)
    case.verify_text(text_input)

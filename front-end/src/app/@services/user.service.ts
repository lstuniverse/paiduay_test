import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _http: Http) { }

  call_all_user() {
    const headers: any = new Headers({
      "Content-Type": "application/json",
      "Cache-Control": "no-cache",
      Pragma: "no-cache",
      Expires: "Sat, 01 Jan 2000 00:00:00 GMT"
    });

    const params = {};

    return this._http
      .get("http://localhost:8000/getall", {
        headers: headers,
        params: params
      })
      .toPromise()
      .then((response: any) => {
        const response_object = JSON.parse(response._body);
        return response_object;
      })
  }

  search_by_id(id) {
    const headers: any = new Headers({
      "Content-Type": "application/json",
      "Cache-Control": "no-cache",
      Pragma: "no-cache",
      Expires: "Sat, 01 Jan 2000 00:00:00 GMT"
    });

    const params = {};

    return this._http
      .get("http://localhost:8000/get/" + id, {
        headers: headers,
        params: params
      })
      .toPromise()
      .then((response: any) => {
        const response_object = JSON.parse(response._body);
        return response_object;
      })
  }

  search_by_fname(name) {
    const headers: any = new Headers({
      "Content-Type": "application/json",
      "Cache-Control": "no-cache",
      Pragma: "no-cache",
      Expires: "Sat, 01 Jan 2000 00:00:00 GMT"
    });

    const params = {};

    return this._http
      .get("http://localhost:8000/getusername/" + name, {
        headers: headers,
        params: params
      })
      .toPromise()
      .then((response: any) => {
        const response_object = JSON.parse(response._body);
        return response_object;
      })
  }

  search_by_lname(lname) {
    const headers: any = new Headers({
      "Content-Type": "application/json",
      "Cache-Control": "no-cache",
      Pragma: "no-cache",
      Expires: "Sat, 01 Jan 2000 00:00:00 GMT"
    });

    const params = {};

    return this._http
      .get("http://localhost:8000/getlastname/" + lname, {
        headers: headers,
        params: params
      })
      .toPromise()
      .then((response: any) => {
        const response_object = JSON.parse(response._body);
        return response_object;
      })
  }

  search_by_email(email) {
    const headers: any = new Headers({
      "Content-Type": "application/json",
      "Cache-Control": "no-cache",
      Pragma: "no-cache",
      Expires: "Sat, 01 Jan 2000 00:00:00 GMT"
    });

    const params = {};

    return this._http
      .get("http://localhost:8000/getemail/" + email, {
        headers: headers,
        params: params
      })
      .toPromise()
      .then((response: any) => {
        const response_object = JSON.parse(response._body);
        return response_object;
      })
  }

  search_by_gender(gender) {
    const headers: any = new Headers({
      "Content-Type": "application/json",
      "Cache-Control": "no-cache",
      Pragma: "no-cache",
      Expires: "Sat, 01 Jan 2000 00:00:00 GMT"
    });

    const params = {};

    return this._http
      .get("http://localhost:8000/getgender/" + gender, {
        headers: headers,
        params: params
      })
      .toPromise()
      .then((response: any) => {
        const response_object = JSON.parse(response._body);
        return response_object;
      })
  }

  search_by_age(age) {
    const headers: any = new Headers({
      "Content-Type": "application/json",
      "Cache-Control": "no-cache",
      Pragma: "no-cache",
      Expires: "Sat, 01 Jan 2000 00:00:00 GMT"
    });

    const params = {};

    return this._http
      .get("http://localhost:8000/getage/" + age, {
        headers: headers,
        params: params
      })
      .toPromise()
      .then((response: any) => {
        const response_object = JSON.parse(response._body);
        return response_object;
      })
  }

  delete_by_id(id) {
    const headers: any = new Headers({
      "Content-Type": "application/json",
      "Cache-Control": "no-cache",
      Pragma: "no-cache",
      Expires: "Sat, 01 Jan 2000 00:00:00 GMT"
    });

    const params = {};

    return this._http
      .delete("http://localhost:8000/delete/" + id, {
        headers: headers,
        params: params
      })
      .toPromise()
      .then((response: any) => {
        const response_object = JSON.parse(response._body);
        return response_object;
      })
  }

  addUser(get_id, get_fname, get_lname, get_email, get_gender, get_age) {
    const headers: any = new Headers({
      "Content-Type": "application/json",
      "Cache-Control": "no-cache",
      Pragma: "no-cache",
      Expires: "Sat, 01 Jan 2000 00:00:00 GMT"
    });

    // get_id = request.form['id']
    // fname = request.form['first_name']
    // lname = request.form['last_name']
    // get_email = request.form['email']
    // get_gender = request.form['gender']
    // get_age = request.form['age']

    // const params = { "id": get_id, "first_name": get_fname, "last_name": get_lname, "email": get_email, "gender": get_gender, "age": get_age };
    // console.log(params);
    return this._http
      .post("http://localhost:8000/create", {
        headers: headers,
        params: { "id": get_id, "first_name": get_fname, "last_name": get_lname, "email": get_email, "gender": get_gender, "age": get_age }
      })
      .toPromise()
      .then((response: any) => {
        const response_object = JSON.parse(response._body);
        return response_object;
      })
  }
}
import { Component, OnInit } from '@angular/core';
import { UserService } from './@services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  all_user: any;
  selected_log: any = 1;
  input_text: any;
  show = true;

  get_id: any;
  get_fname: any;
  get_lname: any;
  get_email: any;
  get_gender: any;
  get_age: any;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.get_all_user();
    this.show = true
  }

  onChange(value) {
    this.show = true
    this.get_all_user();
    this.selected_log = value;
  }

  get_all_user() {
    this.userService.call_all_user().then(data => {
      this.all_user = data;
      // console.log(data);
    })
  }

  search() {
    if (this.input_text == null) {
      this.get_all_user();
    }
    this.show = false;
    if (this.selected_log == 1) {
      this.userService.search_by_id(this.input_text).then(data => {
        if (data) {
          this.all_user = data;
        }
      })
    } else if (this.selected_log == 2) {
      this.userService.search_by_fname(this.input_text).then(data => {
        if (data && data.length == 1) {
          this.all_user = data;
        }
      })
    } else if (this.selected_log == 3) {
      this.userService.search_by_lname(this.input_text).then(data => {
        if (data) {
          this.all_user = data;
        }
      })
    } else if (this.selected_log == 4) {
      this.userService.search_by_email(this.input_text).then(data => {
        if (data) {
          this.all_user = data;
        }
      })
    } else if (this.selected_log == 5) {
      this.userService.search_by_gender(this.input_text).then(data => {
        if (data) {
          this.show = true;
          this.all_user = data;
        }
      })
    } else if (this.selected_log == 6) {
      this.userService.search_by_age(this.input_text).then(data => {
        if (data) {
          this.show = true;
          this.all_user = data;
        }
      })
    }
  }

  delete(id) {
    this.userService.delete_by_id(id).then(data => {
      alert(data);
      this.get_all_user();
    })
  }

  save() {
    this.userService.addUser(this.get_id, this.get_fname, this.get_lname, this.get_email, this.get_gender, this.get_age).then(data => {
      console.log(data)
    })
  }
}
